﻿using System;
using System.Collections.Generic;
using System.Text;
using Tooa.Plc.Hierarchy;

namespace Tooa.Plc
{
    public interface ISoftwareFolder
    {
        IReadOnlyCollection<ISoftwareSubFolder> Folders { get; }

        ICollection<ICodeBlock> CodeBlocks { get; }

        ICollection<IDataBlock> DataBlocks { get; }

        void Add(ITemplate template);

        ISoftwareSubFolder CreateFolder(string name);
    }
}
