﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tooa.Plc
{
    public class SoftwareFactory : ISoftwareFactory
    {
        public ISoftware CreateSoftware()
        {
            return new Software();
        }
    }
}
