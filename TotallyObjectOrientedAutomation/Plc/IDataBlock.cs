﻿namespace Tooa.Plc
{
    public interface IDataBlock
    {
        string Name { get; set; }
    }
}
