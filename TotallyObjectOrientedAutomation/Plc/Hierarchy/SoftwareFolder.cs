﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tooa.Plc.Hierarchy
{
    internal abstract class SoftwareFolder : ISoftwareFolder
    {
        private readonly List<ISoftwareSubFolder> _folders = new List<ISoftwareSubFolder>();
        private readonly List<ICodeBlock> _codeBlocks = new List<ICodeBlock>();
        private readonly List<IDataBlock> _dataBlocks = new List<IDataBlock>();

        public IReadOnlyCollection<ISoftwareSubFolder> Folders => _folders.AsReadOnly();

        public ICollection<ICodeBlock> CodeBlocks => _codeBlocks;

        public ICollection<IDataBlock> DataBlocks => _dataBlocks;

        public void Add(ITemplate template)
        {
            template.InstanciateInside(this);
        }

        public ISoftwareSubFolder CreateFolder(string name)
        {
            var folder = SubFolder.Create(name, this);
            _folders.Add(folder);
            return folder;
        }
    }
}
