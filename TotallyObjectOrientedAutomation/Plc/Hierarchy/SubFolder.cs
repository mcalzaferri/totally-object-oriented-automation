﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tooa.Plc.Hierarchy
{
    internal class SubFolder : SoftwareFolder, ISoftwareSubFolder
    {
        private SubFolder(string name, ISoftwareFolder parent)
        {
            Name = name;
            Parent = parent;
        }

        public string Name { get; }

        public ISoftwareFolder Parent { get; }

        internal static ISoftwareSubFolder Create(string name, ISoftwareFolder parent)
        {
            return new SubFolder(name, parent);
        }
    }
}
