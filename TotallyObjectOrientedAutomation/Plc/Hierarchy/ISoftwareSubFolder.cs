﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tooa.Plc.Hierarchy
{
    public interface ISoftwareSubFolder : ISoftwareFolder
    {
        ISoftwareFolder Parent { get; }

        string Name { get; }
    }
}
