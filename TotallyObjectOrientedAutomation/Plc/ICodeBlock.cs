﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tooa.Plc
{
    public interface ICodeBlock
    {
        string Name { get; set; }
    }
}
