﻿namespace Tooa.Plc
{
    public interface ITemplate
    {
        void InstanciateInside(ISoftwareFolder softwareFolder);
    }
}
