﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tooa.Plc
{
    public class DataBlock : IDataBlock
    {
        public DataBlock(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name), $"{nameof(name)} must not be empty or whitespace");
            }

            Name = name;
        }

        public string Name { get; set; }
    }
}
