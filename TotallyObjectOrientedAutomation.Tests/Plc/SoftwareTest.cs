﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Tooa.Plc
{
    public class SoftwareTest
    {
        private readonly ISoftware _sofware = new SoftwareFactory().CreateSoftware();

        [Fact]
        public void ShouldConstruct()
        {
            Assert.NotNull(_sofware);
        }

        [Fact]
        public void ShouldCreateFolderHierarchy()
        {
            var testFolder = _sofware.CreateFolder("TestFolder");
            Assert.Contains(_sofware.Folders, folder => folder.Name == testFolder.Name);
            var subFolder = testFolder.CreateFolder("TestSubFolder");
            Assert.Contains(_sofware.Folders, folder => folder.Folders.Contains(subFolder));
            Assert.True(subFolder.Parent == testFolder);
        }

        [Theory]
        [InlineData("myInstanceOfTemplate1")]
        [InlineData("myInstanceOfTemplate2")]
        [InlineData("other")]
        public void GeneratedBlocksShouldDependOnTemplateVariables(string name)
        {
            var myTemplate = new MyTestTemplate(name);
            _sofware.Add(myTemplate);
            Assert.Contains(_sofware.CodeBlocks, codeBlock => codeBlock.Name == name);
        }

        [Fact]
        public void AddedTemplateShouldGenerateFunctions()
        {
            var myTemplate = new MyTestTemplate("testName");
            _sofware.Add(myTemplate);
            Assert.Single(_sofware.CodeBlocks);
            Assert.Contains(_sofware.CodeBlocks, codeBlock => codeBlock.Name == myTemplate.GeneratedCodeBlock.Name);
        }

        [Fact]
        public void AddedTemplateShouldGenerateDataBlocks()
        {
            var myTemplate = new MyTestTemplate("testName");
            _sofware.Add(myTemplate);
            Assert.Equal(2, _sofware.DataBlocks.Count);
            Assert.Contains(_sofware.DataBlocks, dataBlock => dataBlock.Name == myTemplate.DataBlock1.Name);
            Assert.Contains(_sofware.DataBlocks, dataBlock => dataBlock.Name == myTemplate.DataBlock2.Name);
        }

        [Fact]
        public void ShouldBeAbleToAddTemplateToFolder()
        {
            var myTemplate = new MyTestTemplate("testName");
            var testFolder = _sofware.CreateFolder("TestFolder");
            testFolder.Add(myTemplate);
            Assert.Single(testFolder.CodeBlocks);
            Assert.Contains(testFolder.CodeBlocks, codeBlock => codeBlock.Name == myTemplate.GeneratedCodeBlock.Name);
        }
    }
}
