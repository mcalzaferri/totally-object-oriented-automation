﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tooa.Plc
{
    public class DataBlockTest
    {
        [Fact]
        public void ShouldThrowWhenCreatingBlockWithInvalidName()
        {
            Assert.ThrowsAny<ArgumentException>(() => new DataBlock(string.Empty));
            Assert.ThrowsAny<ArgumentException>(() => new DataBlock(null));
        }
    }
}
