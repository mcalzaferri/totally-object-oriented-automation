﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tooa.Plc
{
    public class CodeBlockTest
    {
        [Fact]
        public void ShouldThrowWhenCreatingBlockWithInvalidName()
        {
            Assert.ThrowsAny<ArgumentException>(() => new CodeBlock(string.Empty));
            Assert.ThrowsAny<ArgumentException>(() => new CodeBlock(null));
        }
    }
}
