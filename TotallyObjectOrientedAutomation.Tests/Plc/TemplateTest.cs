﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tooa.Plc
{
    public class TemplateTest
    {
        private readonly MyTestTemplate _template;

        public TemplateTest()
        {
            _template = new MyTestTemplate("myTemplate");
        }

        [Fact]
        public void TestTemplateShouldDeriveInterface()
        {
            Assert.IsAssignableFrom<ITemplate>(_template);
        }
    }
}
