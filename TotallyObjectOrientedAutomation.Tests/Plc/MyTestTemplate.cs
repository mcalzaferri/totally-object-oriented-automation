﻿using System.Collections;

namespace Tooa.Plc
{
    internal class MyTestTemplate : ITemplate
    {
        public MyTestTemplate(string name)
        {
            GeneratedCodeBlock = new CodeBlock(name);
            DataBlock1 = new DataBlock(name + "Db1");
            DataBlock2 = new DataBlock(name + "Db2");
        }

        public ICodeBlock GeneratedCodeBlock { get; internal set; }

        public IDataBlock DataBlock1 { get; internal set; }

        public IDataBlock DataBlock2 { get; internal set; }

        public void InstanciateInside(ISoftwareFolder software)
        {
            software.CodeBlocks.Add(GeneratedCodeBlock);
            software.DataBlocks.Add(DataBlock1);
            software.DataBlocks.Add(DataBlock2);
        }
    }
}
